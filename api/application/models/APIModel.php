<?php
class APIModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function CreateUser($user_name, $user_email, $user_password)
    {
        $this->db->where("tbl_mst_user.user_email", $user_email);
        $EmailExsist = $this->db->get("tbl_mst_user");

        $data = array(
            'user_name' => $user_name,
            'user_email' => $user_email,
            'user_password' => $user_password,
            'user_created_date' => date('d-m-Y'),
            'user_update_date' => date('d-m-Y')
        );

        if ($EmailExsist->num_rows() == 0) {
            $this->db->insert("tbl_mst_user", $data);
            return "success";
        }else{
            return "Email has already been used";
        }
    }

    public function UpdateUser($user_id, $user_name, $user_email, $user_password)
    {
        $this->db->where("tbl_mst_user.user_id", $user_id);
        $Exsist = $this->db->get("tbl_mst_user");

        $data = array(
            'user_id' => $user_id,
            'user_name' => $user_name,
            'user_email' => $user_email,
            'user_password' => $user_password,
            'user_update_date' => date('d-m-Y')
        );

        if ($Exsist->num_rows() != 0) {
            $this->db->where("tbl_mst_user.user_id", $user_id);
            $this->db->update("tbl_mst_user", $data);
            return "success";
        }else{
            return "there is no data in the database";
        }
    }

    public function DeleteUser($user_id)
    {
        $this->db->where("tbl_mst_user.user_id", $user_id);
        $Exsist = $this->db->get("tbl_mst_user");

        if ($Exsist->num_rows() != 0) {
            $this->db->where("tbl_mst_user.user_id", $user_id);
            $this->db->delete("tbl_mst_user");
            return "success";
        }else{
            return "there is no data in the database";
        }
    }
}
?>
 